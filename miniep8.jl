using Test

function compareByValue(x, y)
	comp=["2","3","4","5","6","7","8","9","10","J","Q","K","A"]
	ix=1
	iy=1
	sx=chop(x)
	sy=chop(y)
	
	for i in 1:length(comp)
		if sx==comp[i]
			ix=i
		end

		if sy==comp[i]
			iy=i
		end
	end

	if ix<iy
		return true
	else
		return false
	end
end

function compareByValueAndSuit(x, y)
	comp=['♦','♠','♥','♣']
	ix=1
	iy=1
	lx=last(x)
	ly=last(y)
	if lx==ly
		return compareByValue(x, y) 
	end

	for i in 1:length(comp)
		if lx==comp[i]
			ix=i
		end
		if ly==comp[i]
			iy=i
		end
	end
	
	if ix<iy
		return true
	else
		return false
	end
end

function troca(v, i, j)
	aux = v[i]
	v[i] = v[j]
	v[j] = aux
end

function insercao(v)
	tam = length(v)
	
	for i in 2:tam
		j = i
		while j > 1
			if compareByValueAndSuit(v[j], v[j - 1])
				troca(v, j, j - 1)
			else
				break
			end
			j = j - 1
		end
	end	
	return v
end

function teste()
	@test insercao([])==[]
	@test insercao(["A♦"])==["A♦"]
	@test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"])==["10♦","J♠","K♠","A♠","A♠","10♥"]
	@test insercao(["A♠","2♦","A♥","9♦","J♦","2♣"])==["2♦","9♦","J♦","A♠","A♥","2♣"]
	println("Fim dos testes.")
end
